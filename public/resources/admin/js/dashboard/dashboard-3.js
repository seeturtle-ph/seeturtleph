(function($) {
    /* "use strict" */


 var dzChartlist = function(){
	
	var screenWidth = $(window).width();
	
	var sparkBar2 = function () {
        if (jQuery('#turtleConditionChart').length > 0) {
            // Fetch data from the API
            $.post("https://pawican.fusiontechph.com/public/printStrandingReport",
                function (data, status) {
                    var jsonData = JSON.parse(data);

                    // Extract relevant data for the chart
                    var turtleConditionData = jsonData.data.map(function (item) {
                        return item.turtle_condition;
                    });

                    // Count occurrences of each turtle condition
                    var turtleConditionCount = {};
                    turtleConditionData.forEach(function (condition) {
                        turtleConditionCount[condition] = (turtleConditionCount[condition] || 0) + 1;
                    });

                    // Display the total turtle stranding count
                    $("#turtleConditionCount").text(Object.values(turtleConditionCount).reduce((acc, count) => acc + count, 0));

                    // Create a bar chart
                    $("#turtleConditionChart").sparkline(Object.values(turtleConditionCount), {
                        type: "bar",
                        height: "140",
                        width: 100,
                        barWidth: 10,
                        barSpacing: 10,
                        barColor: "rgb(200, 255, 135)"
                    });
                }
            );
        }
    }

    // Call the function to generate the chart
    sparkBar2();	
	
 var sparkLineChart = function () {
        if (jQuery('#nestRecordPieChart').length > 0) {
            // Fetch data from the API
            $.post("https://pawican.fusiontechph.com/public/printNestingData",
                function (data, status) {
                    var jsonData = JSON.parse(data);

                    // Extract relevant data for the chart
                    var collectedByData = jsonData.data.map(function (item) {
                        return item.collected_by;
                    });

                    // Count occurrences of each person who collected nest records
                    var collectedByCount = {};
                    collectedByData.forEach(function (person) {
                        collectedByCount[person] = (collectedByCount[person] || 0) + 1;
                    });

                    // Display the total nest record count
                    $("#nestRecordCount").text(Object.values(collectedByCount).reduce((acc, count) => acc + count, 0));

                    // Create a pie chart
                    $("#nestRecordPieChart").sparkline(Object.values(collectedByCount), {
                        type: "pie",
                        height: "100",
                        sliceColors: ["#8d95ff", "#d7daff", "#c7cbff"]
                    });
                }
            );
        }
    }
    
 // Function to fetch data and update the chart
 var updateHatchlingsReleasedChart = function () {
    // Fetch data from the API
    $.post("https://pawican.fusiontechph.com/public/printHatcheryData",
        function (data, status) {
            console.log(data); // Log the response to the console
            var jsonData = JSON.parse(data);

            // Extract relevant data for the chart
            var hatchlingsReleasedData = jsonData.data.map(function (item) {
                return parseInt(item.hatchlings_released);
            });

            // Calculate the total hatchlings released count
            var totalHatchlingsReleased = hatchlingsReleasedData.reduce(function (acc, count) {
                return acc + count;
            }, 0);

            // Display the total hatchlings released count
            $("#hatchlingsReleasedCount").text(totalHatchlingsReleased);

            // Create a line chart with Sparkline
            $("#hatchlingsReleasedChart").sparkline(hatchlingsReleasedData, {
                type: "line",
                width: "100%",
                height: 100,
                lineColor: "#8045EB", // Set the line color to purple
                fillColor: "#724DB4" // Light purple background
            });
        }
    );
}

// Call the function to update the chart
updateHatchlingsReleasedChart();

// Function to fetch data and update the chart
var updateStrandingOverTimeChart = function () {
    // Fetch data from the API
    $.post("https://pawican.fusiontechph.com/public/printStrandingReport",
        function (data, status) {
            var jsonData = JSON.parse(data);

            // Extract relevant data for the chart
            var strandingData = jsonData.data.map(function (item) {
                return item.stranding_datetime;
            });

            // Count occurrences of each date
            var dateCounts = {};
            strandingData.forEach(function (date) {
                dateCounts[date] = (dateCounts[date] || 0) + 1;
            });

            // Calculate the total number of strandings
            var totalStrandings = Object.values(dateCounts).reduce((acc, count) => acc + count, 0);

            // Display the total number of strandings
            $("#strandingCount").text(totalStrandings);

            // Create an array to represent the count of strandings at each date
            var strandingsCount = Object.values(dateCounts);

            // Create a line chart with Sparkline
            $("#strandingChart").sparkline(strandingsCount, {
                type: "line",
                width: "100%",
                height: 100,
                lineColor: "#FF8F8F", // Set the line color to purple
                fillColor: "#E85454" // Light purple background
            });
        }
    );
}

// Call the function to update the chart
updateStrandingOverTimeChart();


var updateEmergenceEfficiencyChart = function () {
    // Fetch data from the API
    $.post("https://pawican.fusiontechph.com/public/printHatcheryData",
        function (data, status) {
            var jsonData = JSON.parse(data);

            // Extract relevant data for the chart
            var eggsTransplanted = jsonData.data.map(function (item) {
                return item.eggs_transplanted || 0;
            });

            var hatchlingsEmerged = jsonData.data.map(function (item) {
                return item.hatchlings_emerged || 0;
            });

            var hatchlingsReleased = jsonData.data.map(function (item) {
                return item.hatchlings_released || 0;
            });

            // Calculate emergence efficiency (percentage)
            var efficiency = hatchlingsEmerged[0] / eggsTransplanted[0] * 100;

            // Display the emergence efficiency count
            $("#efficiencyCount").text(efficiency.toFixed(2) + "%");

            // Create a Bullet Chart
            $("#efficiencyChart").sparkline([efficiency, hatchlingsEmerged[0], hatchlingsReleased[0]], {
                type: "bullet",
                targetColor: "blue",
                performanceColor: "green"
            });
        }
    );
}

// Call the function to update the chart
updateEmergenceEfficiencyChart();

	// Function to create the nesting report bar chart
    var barChart = function () {
        if (jQuery('#barChart_2').length > 0) {
            const barChart_2 = document.getElementById("barChart_2").getContext('2d');
            const barChart_2gradientStroke = barChart_2.createLinearGradient(0, 0, 0, 250);
            barChart_2gradientStroke.addColorStop(0, "rgba(141, 149, 255, 1)");
            barChart_2gradientStroke.addColorStop(1, "rgba(102, 115, 253, 1)");

            barChart_2.height = 100;

            // Fetch data from the API
            $.post("https://pawican.fusiontechph.com/public/printNestingData",
                function (data, status) {
                    var jsonData = JSON.parse(data);

                    // Extract relevant data for the chart
                    var number_of_eggs_data = jsonData.data.map(function (item) {
                        return item.number_of_eggs;
                    });

                    var date_transplanted_labels = jsonData.data.map(function (item) {
                        return item.date_transplanted;
                    });

                    new Chart(barChart_2, {
                        type: 'bar',
                        data: {
                            defaultFontFamily: 'Poppins',
                            labels: date_transplanted_labels,
                            datasets: [
                                {
                                    label: "Number of Eggs",
                                    data: number_of_eggs_data,
                                    borderColor: barChart_2gradientStroke,
                                    borderWidth: "0",
                                    backgroundColor: barChart_2gradientStroke,
                                    hoverBackgroundColor: barChart_2gradientStroke
                                }
                            ]
                        },
                        options: {
                            legend: false,
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }],
                                xAxes: [{
                                    barPercentage: 0.5
                                }]
                            }
                        }
                    });
                }
            );
        }
    };

    // Function to create the stranded turtle report bar chart
    var barChartSpecies = function () {
        if (jQuery('#barChart_species').length > 0) {
            const barChartSpecies = document.getElementById("barChart_species").getContext('2d');
            const barChartSpeciesGradientStroke = barChartSpecies.createLinearGradient(0, 0, 0, 250);
            barChartSpeciesGradientStroke.addColorStop(0, "rgba(141, 149, 255, 1)");
            barChartSpeciesGradientStroke.addColorStop(1, "rgba(102, 115, 253, 1)");

            barChartSpecies.height = 100;

            // Fetch data from the API
            $.post("https://pawican.fusiontechph.com/public/printStrandingReport",
                function (data, status) {
                    var jsonData = JSON.parse(data);

                    // Extract relevant data for the chart
                    var speciesData = jsonData.data.map(function (item) {
                        return item.species;
                    });

                    var speciesCount = {};
                    speciesData.forEach(function (species) {
                        speciesCount[species] = (speciesCount[species] || 0) + 1;
                    });

                    var speciesLabels = Object.keys(speciesCount);
                    var speciesCountData = Object.values(speciesCount);

                    new Chart(barChartSpecies, {
                        type: 'bar',
                        data: {
                            defaultFontFamily: 'Poppins',
                            labels: speciesLabels,
                            datasets: [
                                {
                                    label: "Number of Turtles",
                                    data: speciesCountData,
                                    borderColor: barChartSpeciesGradientStroke,
                                    borderWidth: "0",
                                    backgroundColor: barChartSpeciesGradientStroke,
                                    hoverBackgroundColor: barChartSpeciesGradientStroke
                                }
                            ]
                        },
                        options: {
                            legend: false,
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }],
                                xAxes: [{
                                    barPercentage: 0.5
                                }]
                            }
                        }
                    });
                }
            );
        }
    };

	var pieChartSex = function () {
        if (jQuery('#pieChart_sex').length > 0) {
            // Fetch data from the API
            $.post("https://pawican.fusiontechph.com/public/printStrandingReport",
                function (data, status) {
                    var jsonData = JSON.parse(data);

                    // Extract relevant data for the chart
                    var sexData = jsonData.data.map(function (item) {
                        return item.sex;
                    });

                    var sexCount = {};
                    sexData.forEach(function (sex) {
                        sexCount[sex] = (sexCount[sex] || 0) + 1;
                    });

                    var sexLabels = Object.keys(sexCount);
                    var sexCountData = Object.values(sexCount);

                    new Chart("pieChart_sex", {
                        type: 'doughnut',
                        data: {
                            labels: sexLabels,
                            datasets: [{
                                data: sexCountData,
                                backgroundColor: ["#8d95ff", "#c7cbff", "#a3a3ff"], // Three colors for the pie chart
                                hoverBackgroundColor: ["#8d95ff", "#c7cbff", "#a3a3ff"] // Hover colors
                            }]
                        },
                        options: {
                            legend: {
                                display: true,
                                position: 'bottom', // You can adjust the position as needed
                                labels: {
                                    fontColor: '#333', // You can customize the font color
                                    fontSize: 12
                                }
                            }
                        }
                    });
                }
            );
        }
    };

	var lineChartStrandings = function () {
        if (jQuery('#lineChart_strandings').length > 0) {
            // Fetch data from the API
            $.post("https://pawican.fusiontechph.com/public/printStrandingReport",
                function (data, status) {
                    var jsonData = JSON.parse(data);

                    // Extract relevant data for the chart
                    var strandingData = jsonData.data.map(function (item) {
                        return {
                            datetime: item.stranding_datetime,
                            count: 1 // Each entry represents one stranding
                        };
                    });

                    // Group data points by stranding datetime
                    var groupedData = {};
                    strandingData.forEach(function (entry) {
                        var datetime = entry.datetime;
                        if (!groupedData[datetime]) {
                            groupedData[datetime] = 0;
                        }
                        groupedData[datetime]++;
                    });

                    // Convert grouped data to arrays for labels and data
                    var datetimeLabels = Object.keys(groupedData);
                    var strandingCountData = Object.values(groupedData);

                    new Chart("lineChart_strandings", {
                        type: 'line',
                        data: {
                            labels: datetimeLabels,
                            datasets: [{
                                label: "Number of Strandings",
                                data: strandingCountData,
                                borderColor: 'rgba(102, 115, 253, 1)',
                                borderWidth: "3",
                                backgroundColor: 'rgba(102, 115, 253, .2)',
                                pointBackgroundColor: 'rgba(102, 115, 253, 1)'
                            }]
                        },
                        options: {
                            legend: false,
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        max: Math.max(...strandingCountData) + 1,
                                        min: 0,
                                        stepSize: 1,
                                        padding: 10
                                    }
                                }],
                                xAxes: [{
                                    ticks: {
                                        padding: 5
                                    }
                                }]
                            }
                        }
                    });
                }
            );
        }
    };

	var stackedBarChartTurtles = function () {
        if (jQuery('#stackedBarChart_turtles').length > 0) {
            // Fetch data from the API
            $.post("https://pawican.fusiontechph.com/public/printStrandingReport",
                function (data, status) {
                    var jsonData = JSON.parse(data);

                    // Extract relevant data for the chart
                    var turtlesData = jsonData.data.map(function (item) {
                        return {
                            species: item.species,
                            sex: item.sex
                        };
                    });

                    // Group data points by species and sex
                    var groupedData = {};
                    turtlesData.forEach(function (entry) {
                        var key = entry.species + '-' + entry.sex;
                        if (!groupedData[key]) {
                            groupedData[key] = 0;
                        }
                        groupedData[key]++;
                    });

                    // Convert grouped data to arrays for labels and data
                    var labels = [...new Set(turtlesData.map(item => item.species))];
                    var datasets = [];
                    var sexValues = [...new Set(turtlesData.map(item => item.sex))];

                    sexValues.forEach(function (sex) {
                        var data = labels.map(function (species) {
                            var key = species + '-' + sex;
                            return groupedData[key] || 0;
                        });

                        datasets.push({
                            label: sex,
                            data: data,
                            backgroundColor: sex === 'Male' ? 'rgba(54, 162, 235, 0.7)' : 'rgba(102, 115, 253, 0.7)',
                            borderColor: sex === 'Male' ? 'rgba(54, 162, 235, 1)' : 'rgba(102, 115, 253, 1)',
                            borderWidth: 1
                        });
                    });

                    new Chart("stackedBarChart_turtles", {
                        type: 'bar',
                        data: {
                            labels: labels,
                            datasets: datasets
                        },
                        options: {
                            legend: {
                                display: true,
                                position: 'top'
                            },
                            scales: {
                                xAxes: [{
                                    stacked: true
                                }],
                                yAxes: [{
                                    stacked: true,
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
                }
            );
        }
    };

	var radarChartObserverContributions = function () {
        if (jQuery('#radarChart_observerContributions').length > 0) {
            // Fetch data from the API
            $.post("https://pawican.fusiontechph.com/public/printStrandingReport",
                function (data, status) {
                    var jsonData = JSON.parse(data);

                    // Extract relevant data for the chart
                    var contributionsData = jsonData.data.map(function (item) {
                        return {
                            observer: item.observer_name,
                            strandings: 1 // Each entry represents one stranding reported
                        };
                    });

                    // Group data points by observer
                    var groupedData = {};
                    contributionsData.forEach(function (entry) {
                        var observer = entry.observer;
                        if (!groupedData[observer]) {
                            groupedData[observer] = 0;
                        }
                        groupedData[observer]++;
                    });

                    // Convert grouped data to arrays for labels and data
                    var observerLabels = Object.keys(groupedData);
                    var contributionsCountData = Object.values(groupedData);

                    new Chart("radarChart_observerContributions", {
                        type: 'radar',
                        data: {
                            labels: observerLabels,
                            datasets: [{
                                label: 'Observer Contributions',
                                data: contributionsCountData,
                                backgroundColor: 'rgba(54, 162, 235, 0.7)',
                                borderColor: 'rgba(54, 162, 235, 1)',
                                borderWidth: 1
                            }]
                        },
                        options: {
                            scale: {
                                ticks: {
                                    beginAtZero: true,
                                    max: Math.max(...contributionsCountData) + 1
                                }
                            }
                        }
                    });
                }
            );
        }
    };

    // Call the function to generate the chart
    radarChartObserverContributions();

    // Call the function to generate the chart
    stackedBarChartTurtles();

    // Call the function to generate the chart
    lineChartStrandings();

    // Call the function to generate the chart
    pieChartSex();

    // Call the functions to generate the charts
    barChart();
    barChartSpecies();


	$(document).ready(function () {
		// Fetch data from the API
		$.post("http://localhost/apiClone/api/public/printName",
			function (data, status) {
				var json = JSON.parse(data);
				var counts = {};
	
				// Count the occurrences of each fname
				json.data.forEach(function (item) {
					counts[item.fname] = (counts[item.fname] || 0) + 1;
				});
	
				// Prepare data for Chart.js
				var labels = Object.keys(counts);
				var data = Object.values(counts);
	
				// Create the bar chart
				var ctx = document.getElementById("fnameChart").getContext('2d');
				var myBarChart = new Chart(ctx, {
					type: 'bar',
					data: {
						labels: labels,
						datasets: [{
							label: 'Number of occurrences',
							data: data,
							backgroundColor: 'rgba(75, 192, 192, 0.7)',
							borderColor: 'rgba(75, 192, 192, 1)',
							borderWidth: 1
						}]
					},
					options: {
						scales: {
							y: {
								beginAtZero: true
							}
						}
					}
				});
			}
		);
	});


	var areaChart = function(){
		if(jQuery('#areaChart_1').length > 0 ){
			const areaChart_1 = document.getElementById("areaChart_1").getContext('2d');
			
			areaChart_1.height = 100;

			new Chart(areaChart_1, {
				type: 'line',
				data: {
					defaultFontFamily: 'Poppins',
					labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
					datasets: [
						{
							label: "My First dataset",
							data: [25, 20, 60, 41, 66, 45, 80],
							borderColor: 'rgba(102, 115, 253, 1)',
							borderWidth: "3",
							backgroundColor: 'rgba(102, 115, 253, .2)', 
							pointBackgroundColor: 'rgba(102, 115, 253, 1)'
						}
					]
				},
				options: {
					legend: false, 
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true, 
								max: 100, 
								min: 0, 
								stepSize: 20, 
								padding: 10
							}
						}],
						xAxes: [{ 
							ticks: {
								padding: 5
							}
						}]
					}
				}
			});
		}
	}

		
	
	/* Function ============ */
		return {
			init:function(){
			},
			
			
			load:function(){
				sparkBar2();
				sparkLineChart();
				barChart();
				areaChart();
			},
			
			resize:function(){
				sparkBar2();
				sparkLineChart();
				barChart();
				areaChart();
			}
		}
	
	}();

	var direction =  getUrlParams('dir');
	if(direction != 'rtl')
	{direction = 'ltr'; }

	var dlabSettingsOptions = {
		typography: "roboto",
		version: "light",
		layout: "Vertical",
		headerBg: "color_1",
		navheaderBg: "color_11",
		sidebarBg: "color_1",
		sidebarStyle: "full",
		sidebarPosition: "fixed",
		headerPosition: "fixed",
		containerLayout: "full",
		direction: direction
	};
	
	jQuery(document).ready(function(){
		new dlabSettings(dlabSettingsOptions); 
		
	});
		
	jQuery(window).on('load',function(){
		dzChartlist.load();
	});

	jQuery(window).on('resize',function(){
		dzChartlist.resize();
		new dlabSettings(dlabSettingsOptions); 
	});     
	

})(jQuery);