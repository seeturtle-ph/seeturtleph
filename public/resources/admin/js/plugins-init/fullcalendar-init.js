!function(e) {
    "use strict";
    var t = function() {
        this.$body = e("body"), this.$modal = e("#event-modal"), this.$event = "#external-events div.external-event", this.$calendar = e("#calendar"), this.$saveCategoryBtn = e(".save-category"), this.$categoryForm = e("#add-category form"), this.$extEvents = e("#external-events"), this.$calendarObj = null
    };

    t.prototype.onDrop = function(t, n) {
        var a = t.data("eventObject"),
            o = t.attr("data-class"),
            i = e.extend({}, a);
        i.start = n, o && (i.className = [o]), this.$calendar.fullCalendar("renderEvent", i, !0), e("#drop-remove").is(":checked") && t.remove()
    };

    t.prototype.onEventClick = function(t, n, a) {
        var o = this,
            i = e("<form></form>");
        i.append("<label>Change event name</label>"), i.append("<div class='input-group'><input class='form-control' type=text value='" + t.title + "' /><span class='input-group-btn'><button type='submit' class='btn btn-success waves-effect waves-light'><i class='fa fa-check'></i> Save</button></span></div>"), o.$modal.modal({
            backdrop: "static"
        }), o.$modal.find(".delete-event").show().end().find(".save-event").hide().end().find(".modal-body").empty().prepend(i).end().find(".delete-event").unbind("click").on("click", function() {
            o.$calendarObj.fullCalendar("removeEvents", function(e) {
                return e._id == t._id
            }), o.$modal.modal("hide")
        }), o.$modal.find("form").on("submit", function() {
            return t.title = i.find("input[type=text]").val(), o.$calendarObj.fullCalendar("updateEvent", t), o.$modal.modal("hide"), !1
        })
    };

    t.prototype.onSelect = function(start, end, allDay) {
        var o = this;
        o.$modal.modal({
            backdrop: "static"
        });

        // Create a new form element
        var i = e("<form id='eventForm'>");

        // Append the first column with Event Title input
        i.append("<div class='row'></div");
        i.find(".row").append(
            "<div class='col-md-6'>" +
                "<div class='form-group'>" +
                    "<label class='control-label'>Event Title</label>" +
                    "<input class='form-control' id='event_title' placeholder='Insert Event Title' type='text' name='title'/>" +
                "</div>" +
            "</div>"
        );

        // Append the second column with Category selection dropdown
        i.find(".row").append(
            "<div class='col-md-6'>" +
                "<div class='form-group'>" +
                    "<label class='control-label'>Category</label>" +
                    "<select class='form-control' name='category' id='event_category'></select>" +
                "</div>" +
            "</div>"
        );

        // Populate the Category dropdown with options
        i.find("select[name='category']").append(
            "<option value='bg-danger'>Danger</option>" +
            "<option value='bg-success'>Success</option>" +
            "<option value='bg-dark'>Dark</option>" +
            "<option value='bg-primary'>Primary</option>" +
            "<option value='bg-pink'>Pink</option>" +
            "<option value='bg-info'>Info</option>" +
            "<option value='bg-warning'>Warning</option>"
        );

        i.find(".row").append(
            "<div class='col-md-12'>" +
                "<div class='form-group'>" +
                    "<label class='control-label'>Event Description</label>" +
                   "<textarea class='form-control' id='event_description' placeholder='Insert Event Description...' type='text' name='title' required></textarea><br>" +
                "</div>" +
            "</div>"
        );

         // Append the date input field
         i.append(
            "<div class='row'>" +
                "<div class='col-md-12'>" +
                    "<div class='form-group'>" +
                        "<label class='control-label'>Date</label>" +
                        "<input type='date' class='form-control' id='eventDate' name='eventDate' readonly required/>" +
                    "</div>" +
                "</div>" +
            "</div>"
        );

        // Set the initial value for the eventDate input
        i.find("#eventDate").val(start.format("YYYY-MM-DD"));

        // Update modal content and behavior
        o.$modal.find(".delete-event").hide()
            .end().find(".save-event").show()
            .end().find(".modal-body").empty().prepend(i)
            .end().find(".save-event").unbind("click")
            .on("click", function() {
                // Get the selected date
                var selectedDate = i.find("#eventDate").val();

                // Get other form values
                var e = i.find("input[name='title']").val(),
                    a = i.find("select[name='category'] option:checked").val();

                // Perform some action when the save-event button is clicked
                if (null !== e && 0 != e.length) {
                    // Render the event with the selected date
                    o.$calendarObj.fullCalendar("renderEvent", {
                        title: e,
                        start: selectedDate,
                        end: end,
                        allDay: !1,
                        className: a
                    }, !0);

                    o.$modal.modal("hide");
                } else {
                    alert("You have to give a title to your event");
                }
            });

        o.$calendarObj.fullCalendar("unselect");
    };

    t.prototype.enableDrag = function() {
        e(this.$event).each(function() {
            var t = {
                title: e.trim(e(this).text())
            };
            e(this).data("eventObject", t), e(this).draggable({
                zIndex: 999,
                revert: !0,
                revertDuration: 0
            })
        })
    };

    t.prototype.init = function() {
        this.enableDrag();
        var t = new Date,
            n = (t.getDate(), t.getMonth(), t.getFullYear(), new Date(e.now())),
            a = [];  // Empty array for events

        var o = this;
        o.$calendarObj = o.$calendar.fullCalendar({
            slotDuration: "00:15:00",
            minTime: "08:00:00",
            maxTime: "19:00:00",
            defaultView: "month",
            handleWindowResize: !0,
            height: e(window).height() - 100,
            header: {
                left: "prev,next today",
                center: "title",
                right: "month,agendaWeek,agendaDay"
            },
            events: a,  // Pass the empty events array
            editable: !0,
            droppable: !0,
            eventLimit: !0,
            selectable: !0,
            drop: function(t) {
                o.onDrop(e(this), t)
            },
            select: function(start, end, allDay) {
                o.onSelect(start, end, allDay)
            },
            eventClick: function(e, t, n) {
                o.onEventClick(e, t, n)
            }
        });

        // Fetch events from the getEvents endpoint
       // ... (previous code)

// Fetch events from the getEvents endpoint
$.get("http://localhost/pawican/public/getEvents", function (data, status) {
    try {
        var events = JSON.parse(data);

        // Assuming each event has a 'category' property
        events.forEach(function (event) {
            switch (event.category) {
                case 'bg-danger':
                case 'bg-success':
                case 'bg-dark':
                case 'bg-primary':
                case 'bg-pink':
                case 'bg-info':
                case 'bg-warning':
                    break;
                default:
                    // Set a default category here
                    event.category = 'bg-dark';
            }

            o.$calendarObj.fullCalendar("renderEvent", event, !0);
        });
    } catch (error) {
        console.error("Error parsing server response:", error);
        alert("An error occurred while parsing events. Please check the console for details.");
    }
}).fail(function(xhr, status, error) {
    console.error("Failed to fetch events from the server. Status:", status, "Error:", error);
    alert("An error occurred while fetching events. Please check the console for details.");
});

// ... (remaining code)


        this.$saveCategoryBtn.on("click", function() {
            var e = o.$categoryForm.find("input[name='category-name']").val(),
                t = o.$categoryForm.find("select[name='category-color']").val();
            null !== e && 0 != e.length && (o.$extEvents.append('<div class="external-event ' + t + '" data-class="' + t + '" style="position: relative;"><i class="fa fa-move"></i>' + e + "</div>"), o.enableDrag())
        });
    };

    e.CalendarApp = new t, e.CalendarApp.Constructor = t
}(window.jQuery),
function(e) {
    "use strict";
    e.CalendarApp.init()
}(window.jQuery);
