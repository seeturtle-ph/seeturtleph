@extends('components.layouts.home')


@section('content')
<main id="main">
    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts">
      <div class="container" data-aos="fade-up">
<br><br>
        <div class="row gy-4">

                    <!-- Your existing HTML -->
            <div class="col-lg-4 col-md-6">
                <div class="count-box">
                    <i class="bi bi-bar-chart" style="color: #186ac7;"></i>
                    <div>
                        <!-- This span will dynamically display the total hatchlings released -->
                        <span id="hatchlingsReleasedCount" class=""></span>
                        <p>Total Hatchlings Released</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="count-box">
                    <i class="bi bi-bar-chart" style="color: #ee6c20;"></i>
                    <div>
                        <!-- This span will dynamically display the total nesting data count -->
                        <span id="nestingDataCount" class=""></span>
                        <p>Total Nesting Data</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="count-box">
                    <i class="bi bi-bar-chart" style="color: #08bb4d;"></i>
                    <div>
                        <!-- This span will dynamically display the total active government users count -->
                        <span id="governmentUsersCount" class=""></span>
                        <p>Total Turtle Stranding</p>
                    </div>
                </div>
            </div>

        </div>

      </div>
    </section><!-- End Counts Section -->


    <!-- ======= Services Section ======= -->
    <section id="services" class="services">

      <div class="container" data-aos="fade-up">

        <header class="section-header">
          <h2>Services</h2>
          <p>Explore the services offered by CURMA</p>
        </header>

        <div class="row gy-4">

          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
            <div class="service-box blue">
              <i class="ri-discuss-line icon"></i>
              <h3>Beach Patrol Services</h3>
              <p>Our volunteers undergo comprehensive training to identify and secure nests, ensuring the safety of marine turtle eggs.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="300">
            <div class="service-box orange">
              <i class="ri-discuss-line icon"></i>
              <h3>Hatchery Management</h3>
              <p>Participate in the protection of turtle eggs by assisting in their transfer to the CURMA hatchery.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="400">
            <div class="service-box green">
              <i class="ri-discuss-line icon"></i>
              <h3>Pawikan Release</h3>
              <p>Witness these gentle creatures emerging from their nests, guiding them safely from the hatchery to the sea.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="500">
            <div class="service-box red">
              <i class="ri-discuss-line icon"></i>
              <h3>Coastal Clean-up </h3>
              <p>Volunteers work to keep beaches and rivers free from pollution, contributing to the protection of marine life and the overall well-being of our coastal ecosystems.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="600">
            <div class="service-box purple">
              <i class="ri-discuss-line icon"></i>
              <h3>Information & Education Campaigns</h3>
              <p>Engage in educational activities such as seminars, forums, and river trekking to increase awareness about the interconnectedness of mountains and oceans.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="700">
            <div class="service-box pink">
              <i class="ri-discuss-line icon"></i>
              <h3>Anti-Poaching Volunteer Services</h3>
              <p> Your volunteer efforts play a vital role in the ongoing success of CURMA's mission.</p>
            </div>
          </div>

        </div>

      </div>

    </section><!-- End Services Section -->
  </main><!-- End #main -->
  @include('components.partials.footer')
@endsection
