@extends('components.layouts.home')

@section('content')
<!-- ======= Values Section ======= -->
<section id="values" class="values">
    <div class="container" data-aos="fade-up">

      <header class="section-header">
        <br>
        <h2>Privacy Policy</h2>
        <p>Your privacy is important to us</p>
      </header>

      <div class="row">

         <!-- Privacy Policy - Information Security -->
        <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="800">
            <div class="box">
            <img src="{{ asset('resources/home/images/values-1.png') }}" class="img-fluid" alt="">
            <h3>Information Security</h3>
            <p>We prioritize the security of your personal information. Our systems implement measures to safeguard against unauthorized access, disclosure, alteration, or destruction of data.</p>
            </div>
        </div>

        <!-- Privacy Policy - Data Usage -->
        <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="1000">
            <div class="box">
            <img src="{{ asset('resources/home/images/values-2.png') }}" class="img-fluid" alt="">
            <h3>Data Usage</h3>
            <p>Your data is used solely for the purpose of providing and improving our services. We do not share, sell, or rent your personal information to third parties without your consent.</p>
            </div>
        </div>

        <!-- Privacy Policy - User Rights -->
        <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="1200">
            <div class="box">
            <img src="{{ asset('resources/home/images/values-3.png')}}" class="img-fluid" alt="">
            <h3>User Rights</h3>
            <p>You have the right to access, correct, or delete your personal information. If you have any questions or concerns about our privacy practices, please contact us.</p>
            </div>
        </div>
      </div>

    </div>
  </section><!-- End Values Section -->
  @include('components.partials.footer')
@endsection
