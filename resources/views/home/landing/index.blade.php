@extends('components.layouts.home')

@section('content')
  <!-- ======= Hero Section ======= -->
  <section id="hero" class="hero d-flex align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center">
                <h1 style="font-weight: 900;" data-aos="fade-up">SAVE THE TURTLES, <br> ACT NOW!</h1>
                <h2 data-aos="fade-up" data-aos-delay="400">Turtles are facing numerous threats to their survival,
                    and it is up to all of us to take action to protect them.</h2>
          <div data-aos="fade-up" data-aos-delay="600">
            <div class="text-center text-lg-start">
              <a href="government-login.html" class="btn-get-started scrollto d-inline-flex align-items-center justify-content-center align-self-center">
                <span>Get Started</span>
                <i class="bi bi-arrow-right"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-6 hero-img" data-aos="zoom-out" data-aos-delay="200">
          <img src="{{ asset('resources/home/images/hero-img.png')}}" class="img-fluid" alt="">
        </div>
      </div>
    </div>
  </section><!-- End Hero -->
@endsection
