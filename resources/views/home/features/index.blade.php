@extends('components.layouts.home')

@section('content')
		<main id="main">
      <!-- ======= Features Section ======= -->
      <section id="features" class="features">

              <div class="container" data-aos="fade-up">

                      <header class="section-header">
                              <br><br>
                              <h2>Features</h2>
                              <p>Empowering Conservation Through Technology</p>
                      </header>

                      <div class="row">

                              <div class="col-lg-6">
                                      <img src="{{ asset('resources/home/images/features.png')}}" class="img-fluid" alt="">
                              </div>

                              <div class="col-lg-6 mt-5 mt-lg-0 d-flex">
                                      <div class="row align-self-center gy-4">

                                              <div class="col-md-6" data-aos="zoom-out" data-aos-delay="200">
                                                      <div class="feature-box d-flex align-items-center">
                                                              <i class="bi bi-check"></i>
                                                              <h3>Nest Management</h3>
                                                      </div>
                                              </div>

                                              <div class="col-md-6" data-aos="zoom-out" data-aos-delay="300">
                                                      <div class="feature-box d-flex align-items-center">
                                                              <i class="bi bi-check"></i>
                                                              <h3>Hatchery Management</h3>
                                                      </div>
                                              </div>

                                              <div class="col-md-6" data-aos="zoom-out" data-aos-delay="400">
                                                      <div class="feature-box d-flex align-items-center">
                                                              <i class="bi bi-check"></i>
                                                              <h3>Turtle Stranding Management</h3>
                                                      </div>
                                              </div>

                                              <div class="col-md-6" data-aos="zoom-out" data-aos-delay="500">
                                                      <div class="feature-box d-flex align-items-center">
                                                              <i class="bi bi-check"></i>
                                                              <h3>Government Account Registration</h3>
                                                      </div>
                                              </div>

                                              <div class="col-md-6" data-aos="zoom-out" data-aos-delay="600">
                                                      <div class="feature-box d-flex align-items-center">
                                                              <i class="bi bi-check"></i>
                                                              <h3>User Account Management</h3>
                                                      </div>
                                              </div>

                                              <div class="col-md-6" data-aos="zoom-out" data-aos-delay="700">
                                                      <div class="feature-box d-flex align-items-center">
                                                              <i class="bi bi-check"></i>
                                                              <h3>Data Visualization</h3>
                                                      </div>
                                              </div>

                                      </div>
                              </div>

                      </div> <!-- / row -->

                      <!-- Feature Tabs -->
                      <div class="row feture-tabs" data-aos="fade-up">
                              <div class="col-lg-6">
                                      <h3>Modern Features for Turtle Management</h3>

                                      <!-- Tabs -->
                                      <ul class="nav nav-pills mb-3">
                                              <li>
                                                      <a class="nav-link active" data-bs-toggle="pill" href="#tab1">Nest</a>
                                              </li>
                                              <li>
                                                      <a class="nav-link" data-bs-toggle="pill" href="#tab2">Turtle Stranding</a>
                                              </li>
                                              <li>
                                                      <a class="nav-link" data-bs-toggle="pill" href="#tab3">Hatchery</a>
                                              </li>
                                              <li>
                                                      <a class="nav-link" data-bs-toggle="pill" href="#tab6">Data Visualization</a>
                                              </li>
                                      </ul><!-- End Tabs -->

                                      <!-- Tab Content -->
                                      <div class="tab-content">

                                              <div class="tab-pane fade show active" id="tab1">
                                                      <p>Efficiently manage turtle nests with functionality like tracking locations and recording
                                                              important data for nest evaluation form.</p>
                                                      <!-- Highlights -->
                                                      <div class="d-flex align-items-center mb-2">
                                                              <i class="bi bi-check2"></i>
                                                              <h4>Real-time Nest Tracking</h4>
                                                      </div>
                                                      <p>Track nesting locations in real-time, ensuring accurate monitoring and timely
                                                              intervention for the protection of turtle nests.</p>
                                                      <div class="d-flex align-items-center mb-2">
                                                              <i class="bi bi-check2"></i>
                                                              <h4>Data Logging and Analysis</h4>
                                                      </div>
                                                      <p>Efficiently log and analyze data related to nesting activities, facilitating informed
                                                              decision-making for conservation efforts.</p>
                                              </div><!-- End Tab 1 Content -->

                                              <div class="tab-pane fade show" id="tab2">
                                                      <p>Effectively manage turtle strandings with functionality like incident reporting and
                                                              monitoring.</p>
                                                      <!-- Highlights -->
                                                      <div class="d-flex align-items-center mb-2">
                                                              <i class="bi bi-check2"></i>
                                                              <h4>Stranding Records Monitoring</h4>
                                                      </div>
                                                      <p>Enable government officials to monitor and review turtle stranding records
                                                              systematically. Access to stranding data allows for efficient decision-making and
                                                              intervention in cases of stranded turtles.</p>
                                              </div><!-- End Tab 2 Content -->

                                              <div class="tab-pane fade show" id="tab3">
                                                      <p>Government monitoring of hatchery activities is essential to ensure the proper release of
                                                              turtle eggs into their natural environment. The ability to track egg releases and
                                                              maintain activity logs supports transparency, accountability, and effective conservation
                                                              management.</p>
                                                      <!-- Highlights -->
                                                      <div class="d-flex align-items-center mb-2">
                                                              <i class="bi bi-check2"></i>
                                                              <h4>Egg Release Monitoring</h4>
                                                      </div>
                                                      <p>Empower government authorities with the ability to monitor and track the release of
                                                              turtle eggs from hatcheries. This feature provides real-time insights into the process,
                                                              ensuring the safe release of eggs into their natural habitat.</p>
                                                      <div class="d-flex align-items-center mb-2">
                                                              <i class="bi bi-check2"></i>
                                                              <h4>Hatchery Activity Logs</h4>
                                                      </div>
                                                      <p>Maintain detailed logs of hatchery activities, including egg releases and related events.
                                                              This comprehensive record-keeping assists government officials in overseeing the
                                                              hatchery's contribution to turtle conservation efforts.</p>
                                              </div><!-- End Tab 3 Content -->
                                              <div class="tab-pane fade show" id="tab6">
                                                      <p>Visualize crucial data for informed decision-making and monitoring of turtle conservation
                                                              activities.</p>
                                                      <!-- Highlights -->
                                                      <div class="d-flex align-items-center mb-2">
                                                              <i class="bi bi-check2"></i>
                                                              <h4>Visual Representation of Data</h4>
                                                      </div>
                                                      <p>Present turtle-related data through interactive and visually appealing charts, graphs,
                                                              and maps. Data visualization aids in quickly understanding trends, patterns, and areas
                                                              that require attention.</p>
                                                      <div class="d-flex align-items-center mb-2">
                                                              <i class="bi bi-check2"></i>
                                                              <h4>Customizable Dashboards</h4>
                                                      </div>
                                                      <p>Provide government users with customizable dashboards. Customization enhances user
                                                              experience and ensures quick access to critical data points.</p>
                                              </div><!-- End Tab 6 Content -->

                                      </div>

                              </div>

                              <div class="col-lg-6">
                                      <img src="{{ asset('resources/home/images/features-2.png')}}" class="img-fluid" alt="">
                              </div>

                      </div><!-- End Feature Tabs -->



                      <!-- Feature Icons -->
                      <div class="row feature-icons" data-aos="fade-up">
                              <h3>Explore the Incredible Functionality of the PAWICAN</h3>

                              <div class="row">

                                      <div class="col-xl-4 text-center" data-aos="fade-right" data-aos-delay="100">
                                              <img src="{{ asset('resources/home/images/features-3.png')}}" class="img-fluid p-4" alt="">
                                      </div>

                                      <div class="col-xl-8 d-flex content">
                                              <div class="row align-self-center gy-4">

                                                      <div class="col-md-6 icon-box" data-aos="fade-up">
                                                              <i class="ri-line-chart-line"></i>
                                                              <div>
                                                                      <h4>Efficient Data Monitoring</h4>
                                                                      <p>Monitor turtle nesting, stranding, hatchery activities, and more with intuitive
                                                                              charts and graphs.</p>
                                                              </div>
                                                      </div>

                                                      <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="100">
                                                              <i class="ri-stack-line"></i>
                                                              <div>
                                                                      <h4>Government Oversight</h4>
                                                                      <p>The system facilitates controlled access and permissions for authorized
                                                                              monitoring.</p>
                                                              </div>
                                                      </div>

                                                      <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="200">
                                                              <i class="ri-brush-4-line"></i>
                                                              <div>
                                                                      <h4>Hatchery Management</h4>
                                                                      <p>Ensure the safe contribution of hatcheries to conservation efforts.</p>
                                                              </div>
                                                      </div>

                                                      <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="300">
                                                              <i class="ri-magic-line"></i>
                                                              <div>
                                                                      <h4>Data Visualization</h4>
                                                                      <p>Visualize trends for quick insights, aiding informed decision-making.</p>
                                                              </div>
                                                      </div>

                                                      <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="400">
                                                              <i class="ri-command-line"></i>
                                                              <div>
                                                                      <h4>Secure Account Management</h4>
                                                                      <p>Ensure secure access with robust account registration and management features.
                                                                      </p>
                                                              </div>
                                                      </div>

                                                      <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="500">
                                                              <i class="ri-radar-line"></i>
                                                              <div>
                                                                      <h4>User-Friendly Profiles</h4>
                                                                      <p>Prioritize a seamless experience for government officials accessing the turtle
                                                                              management system.</p>
                                                              </div>
                                                      </div>

                                              </div>
                                      </div>


                              </div>
                      </div>

              </div>

              </div><!-- End Feature Icons -->

              </div>

      </section><!-- End Features Section -->

		</main><!-- End #main -->
    @include('components.partials.footer')
@endsection
