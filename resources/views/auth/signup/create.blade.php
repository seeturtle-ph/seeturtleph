@extends('components.layouts.auth-form')

@php
    $jsonUrl = asset('resources/forms/js/add.json');
@endphp


@section('content')
    <div class="d-lg-flex half">
        <!-- <div class="bg order-1 order-md-2" style="background-image: url('images/features-3.png');"></div> -->
        <div class="bg order-1 order-md-2"> <img style="width: 70%; height: auto;"
                src="{{ asset('resources/forms/images/features-3.png') }}" alt=""></div>
        <div class="contents order-2 order-md-1">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-7 py-5">
                        <h3 style="display: flex; align-items: center; font-size: 40px;">CREATE AN ACCOUNT <a
                                style="width: 10%" href="{{ route('landing.index') }}"> <img
                                    style="width: 100%; align-self: flex-end; margin-left: 50px;"
                                    src="{{ asset('resources/forms/images/home.png') }}"></a></h3>
                        <p class="mb-4">Fill out the following information</p>
                        <form action="{{ route('signup.store')}}" method="post">
                          @csrf
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group first">
                                        <label for="fname">First Name</label>
                                        <input type="text" class="form-control" placeholder="e.g. John" id="fname"
                                            name="fname">
                                        @if ($errors->has('fname'))
                                        <span class="text-danger">{{ $errors->first('fname') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group first">
                                        <label for="mname">Middle Name</label>
                                        <input type="text" class="form-control" placeholder="e.g. Doe" id="mname"
                                            name="mname">
                                        @if ($errors->has('mname'))
                                        <span class="text-danger">{{ $errors->first('mname') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group first">
                                        <label for="lname">Last Name</label>
                                        <input type="text" class="form-control" placeholder="e.g. Smith" id="lname"
                                            name="lname">
                                        @if ($errors->has('lname'))
                                        <span class="text-danger">{{ $errors->first('lname') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group first">
                                        <label for="birthdate">Birthday</label>
                                        <input type="date" class="form-control" id="birthdate" name="birthdate">
                                        @if ($errors->has('birthdate'))
                                        <span class="text-danger">{{ $errors->first('birthdate') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group first">
                                        <label for="gender">Gender</label>
                                        <select type="text" class="form-control" id="gender" name="gender">
                                            <option value="" disabled selected>Gender</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                        @if ($errors->has('gender'))
                                        <span class="text-danger">{{ $errors->first('gender') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group first">
                                        <label for="region">Region</label>
                                        <select type="text" class="form-control" id="region" name="region">
                                            <option value="" disabled selected>Region</option>
                                        </select>
                                        @if ($errors->has('region'))
                                        <span class="text-danger">{{ $errors->first('region') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group first">
                                        <label for="province">Province</label>
                                        <select type="text" class="form-control" id="province" name="province" disabled>
                                            <option value="" disabled selected>Province</option>
                                        </select>
                                        @if ($errors->has('province'))
                                        <span class="text-danger">{{ $errors->first('province') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group first">
                                        <label for="municipality">Municipality</label>
                                        <select type="text" class="form-control" id="municipality" name="municipality"
                                            disabled>
                                            <option value="" disabled selected>Municipality</option>
                                        </select>
                                        @if ($errors->has('municipality'))
                                        <span class="text-danger">{{ $errors->first('municipality') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group first">
                                        <label for="barangay">Barangay</label>
                                        <select type="text" class="form-control" id="barangay" name="barangay" disabled>
                                            <option value="" disabled selected>Barangay</option>
                                        </select>
                                        @if ($errors->has('barangay'))
                                        <span class="text-danger">{{ $errors->first('barangay') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group first">
                                        <label for="contact_number">Contact Number</label>
                                        <input type="text" class="form-control" placeholder="Your Contact Number"
                                            id="contact_number" name="contact_number">
                                        @if ($errors->has('contact_number'))
                                        <span class="text-danger">{{ $errors->first('contact_number') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group first">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control"
                                            placeholder="e.g. john@your-domain.com" id="email" name="email">
                                        @if ($errors->has('email'))
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group last mb-3">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" placeholder="Your Password"
                                            id="password" name="password">
                                        <span class="toggle-password"
                                            onclick="togglePassword('password', 'password-toggle')">
                                            <i id="password-toggle" class="fa fa-eye-slash"></i>
                                        </span>
                                        @if ($errors->has('password'))
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group last mb-3">
                                        <label for="re-password">Re-type Password</label>
                                        <input type="password" class="form-control" placeholder="Your Password"
                                            id="re-password" name="re-password">
                                        <span class="toggle-password"
                                            onclick="togglePassword('re-password', 're-password-toggle')">
                                            <i id="re-password-toggle" class="fa fa-eye-slash"></i>
                                        </span>
                                        @if ($errors->has('re-password'))
                                        <span class="text-danger">{{ $errors->first('re-password') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex mb-5 mt-4 align-items-center">
                                <div class="d-flex align-items-center">
                                    <label class="control control--checkbox mb-0"><span class="caption">Creating an
                                            account means you're
                                            okay with our <a href="#">Privacy Policy</a>.</span>
                                        <input type="checkbox" checked="checked" disabled />
                                        <div class="control__indicator"></div>
                                    </label>
                                </div>
                            </div>
                            <p class="mb-4">Already have an account? <a href="{{ route('login') }}">Login Here</a>
                            </p>
                            <input type="submit" value="Sign Up" class="btn px-5 btn-primary">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
      try {
          const jsonUrl = @json($jsonUrl);
          console.log("JSON URL:", jsonUrl);

          fetch(jsonUrl)
              .then(response => response.json())
              .then(addresses => {
                  console.log("Addresses:", addresses);
                  initializeDropdowns(addresses);
              })
              .catch(error => console.error("Error fetching JSON:", error));
      } catch (error) {
          console.error("Error:", error);
      }
  </script>
@endsection
