@extends('components.layouts.auth-form')

@section('content')
<div class="d-lg-flex half">
    <!-- <div class="bg order-1 order-md-2" style="background-image: url('images/features-3.png');"></div> -->
    <div class="bg order-1 order-md-2"> <img style="width: 70%; height: auto;" src="{{ asset('resources/forms/images/features-3.png')}}" alt=""></div>
    <div class="contents order-2 order-md-1">
      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-7 py-5">
            <h3 style="display: flex; align-items: center; font-size: 40px;">WELCOME BACK! <a style="width: 10%" href="{{ route('landing.index')}}" > <img  style="width: 100%; align-self: flex-end; margin-left: 50px;" src="{{ asset('resources/forms/images/home.png')}}"></a></h3>
            <p style="color: black;" class="mb-4">Login to continue</p>
            <form action="{{ route('login.authenticate') }}" method="post">
            @csrf
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group first">
                    <label for="email">Email Address</label>
                    <input type="email" class="form-control" placeholder="e.g. john@your-domain.com" id="email" name="email">
                    @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group first">
                    <label for="email">Password</label>
                    <label for="password">Password</label>
                      <input type="password" class="form-control" placeholder="Your Password"
                          id="password" name="password">
                      <span class="toggle-password"
                          onclick="togglePassword('password', 'password-toggle')">
                          <i id="password-toggle" class="fa fa-eye-slash"></i>
                      </span>
                    @if ($errors->has('password'))
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <p class="mb-4">Don't have an account? <a href="{{ route('signup.create')}}">Sign Up Here</a></p>
              <input type="submit" value="Login Now" class="btn px-5 btn-primary">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
