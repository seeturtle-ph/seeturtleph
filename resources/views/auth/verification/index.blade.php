
@extends('components.layouts.auth-form')

@section('content')
<div class="d-lg-flex half">
    <div class="d-lg-flex half">
        <!-- <div class="bg order-1 order-md-2" style="background-image: url('images/features-3.png');"></div> -->
        <div class="bg order-1 order-md-2"> <img style="width: 100%; height: auto;" src="{{ asset('resources/forms/images/verify-email.png')}}" alt=""></div>
        <div class="contents order-2 order-md-1">
          <div class="container">
            <div class="row align-items-center justify-content-center">
              <div class="col-md-7 py-5">
                <h3 style="display: flex; align-items: center; font-size: 40px;">ACCOUNT VERIFICATION SUCCESSFULL<a style="width: 10%" href="{{ route('landing.index')}}" ><img  style="width: 100%; align-self: flex-end; margin-left: 50px;" src="{{ asset('resources/forms/images/home.png')}}"></a></h3>
                <p style="color: black;" class="mb-4">Your Account Was Successfully Verified</p>
                <p style="color: red;" class="mb-4">Note: Check your SPAM messages if the code doesn't appear in your inbox</p>
                <p class="mb-4">Don't have an account? <a href="{{ route('signup.create')}}">Sign Up Here</a></p>
                <p>You will be redirected to the login page in <span id="redirectTimer">5</span> seconds.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <script>
        // Redirect to login page after 5 seconds
        setTimeout(function() {
            window.location.href = "{{ route('login') }}";
        }, 5000);

        // Update countdown timer every second
        var countdown = 5;
        var redirectTimer = document.getElementById('redirectTimer');
        setInterval(function() {
            countdown--;
            redirectTimer.innerText = countdown;
        }, 1000);
    </script>
@endsection
