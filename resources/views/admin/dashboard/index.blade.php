<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>PAWICAN - Dashboard </title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../images/favicon-pawican.png">
    <link rel="stylesheet" href="{{ asset('resources/admin/vendor/jqvmap/css/jqvmap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('resources/admin/vendor/chartist/css/chartist.min.css')}}">
	<link rel="stylesheet" href="{{ asset('resources/admin//vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{ asset('resources/admin/css/skin-2.css')}}">
	<link rel="stylesheet" href="{{ asset('resources/admin/css/style.css')}}">

    <!-- JQuery -->
    <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js')}}"></script>
    <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.js')}}"></script>


</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="index.html" class="brand-logo">
                <img class="logo-abbr" src="../images/logo-pawican.png" alt="">
                <img class="logo-compact" src="../images/logo-text-pawican.png" alt="">
                <img class="brand-title" src="../images/logo-text-pawican.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                            <div class="search_bar dropdown">
                                <span class="search_icon p-3 c-pointer" data-toggle="dropdown">
                                    <i class="mdi mdi-magnify"></i>
                                </span>
                                <div class="dropdown-menu p-0 m-0">
                                    <form>
                                        <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                                    </form>
                                </div>
                            </div>
                        </div>

                        <ul class="navbar-nav header-right">
                            <li class="nav-item dropdown header-profile">
                                <a class="nav-link" href="#" role="button" data-toggle="dropdown">
                                    <img src="../images/profile-icon2.png" width="20" alt="">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="profile.html" class="dropdown-item ai-icon">
                                        <svg id="icon-user1" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                        <span class="ml-2">Profile </span>
                                    </a>
                                    <a class="dropdown-item ai-icon" id="logout">
                                        <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
                                        <span class="ml-2">Logout </span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="dlabnav">
            <div class="dlabnav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="nav-label first">Main Menu</li>
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">
							<i class="la la-home"></i>
							<span class="nav-text">Dashboard</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="index.html">Overview</a></li>
                            <li><a href="analytics.html">Analytics</a></li>
                        </ul>
                    </li>

                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">
                            <i class="la la-signal"></i>
                            <span class="nav-text">Nesting</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="add-nesting.html">Add Nest</a></li>
                            <li><a href="all-nesting.html">Nest Record List</a></li>
                        </ul>
                    </li>

                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="la la-building"></i>
                        <span class="nav-text">Hatchery</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="add-hatchery.html">Create Hatchery Data</a></li>
                            <li><a href="all-hatchery.html">Hatchery Record List</a></li>
                        </ul>
                    </li>
                    <li class="nav-label first">Stranding Reports</li>
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="la la-globe"></i>
                        <span class="nav-text">Turtle Stranding</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="add-stranding.html">Add Stranding Report</a></li>
                            <li><a href="all-stranding.html">Stranding Reports</a></li>
                        </ul>
                    </li>
                    <li class="nav-label first">Users</li>
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="la la-users"></i>
                        <span class="nav-text">User Management</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="add-account.html">Add Account</a></li>
                            <li><a href="all-accounts.html">Government Accounts</a></li>
                        </ul>
                    </li>
				</ul>
            </div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <!-- row -->
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">
                        <div class="welcome-text">
                            <h4><div id="userCredentials"></div></h4>
                            <span class="ml-1" id="designation"></span>
                        </div>
                    </div>
                </div>

                <div class="row">
					<div class="col-xl-3 col-xxl-3 col-sm-6">
                        <div class="widget-stat card bg-primary overflow-hidden">
                            <div class="card-header">
                                <h3 class="card-title text-white">Total Nest Records</h3>
                                <h5 class="text-white mb-0"><i class="fa fa-caret-up"></i> <span id="nestRecordCount">Loading...</span></h5>
                            </div>
                            <div class="card-body text-center mt-3">
                                <div class="ico-sparkline">
                                    <div id="nestRecordPieChart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="col-xl-3 col-xxl-3 col-sm-6">
                        <div class="widget-stat card bg-success overflow-hidden">
                            <div class="card-header">
                                <h3 class="card-title text-white">Total Stranding Reports</h3>
                                <h5 class="text-white mb-0"><i class="fa fa-caret-up"></i> <span id="turtleConditionCount">Loading...</span></h5>
                            </div>
                            <div class="card-body text-center mt-4 p-0">
                                <div class="ico-sparkline">
                                    <div id="turtleConditionChart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-xxl-3 col-sm-6">
                        <div class="widget-stat card bg-secondary overflow-hidden">
                            <div class="card-header">
                                <h3 class="card-title text-white">Total Hatchlings Released</h3>
                                <h5 class="text-white mb-0"><i class="fa fa-caret-up"></i> <span id="hatchlingsReleasedCount">Loading...</span></h5>
                            </div>
                            <div class="card-body text-center mt-4 p-0">
                                <div class="ico-sparkline">
                                    <div id="hatchlingsReleasedChart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-xxl-3 col-sm-6">
                        <div class="widget-stat card bg-danger overflow-hidden">
                            <div class="card-header">
                                <h3 class="card-title text-white">Turtle Stranding Over Time</h3>
                                <h5 class="text-white mb-0"><i class="fa fa-caret-up"></i> <span id="strandingCount">Loading...</span></h5>
                            </div>
                            <div class="card-body text-center mt-4 p-0">
                                <div class="ico-sparkline">
                                    <div id="strandingChart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="col-xl-6 col-xxl-6 col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Turtle Nesting Report</h3>
                            </div>
                            <div class="card-body">
                                <canvas id="barChart_2"></canvas>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6 col-xxl-6 col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Stranded Turtles Based on Species</h3>
                            </div>
                            <div class="card-body">
                                <canvas id="barChart_species"></canvas>
                            </div>
                        </div>
                    </div>

				</div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © Designed &amp; Developed by <a href="#" target="_blank">4ECore</a> 2024</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

		<!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->


    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="{{ asset('resources/admin/vendor/global/global.min.js') }}"></script>
	<script src="{{ asset('resources/admin/vendor/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
    <script src="{{ asset('resources/admin/js/custom.min.js')}}"></script>
	<script src="{{ asset('resources/admin/js/dlabnav-init.js')}}"></script>

    <!-- Chart ChartJS plugin files -->
    <script src="{{ asset('resources/admin/vendor/chart.js/Chart.bundle.min.js')}}"></script>

	<!-- Chart piety plugin files -->
    <script src="{{ asset('resources/admin/vendor/peity/jquery.peity.min.js')}}"></script>

	<!-- Chart sparkline plugin files -->
    <script src="{{ asset('resources/admin/vendor/jquery-sparkline/jquery.sparkline.min.js')}}"></script>

		<!-- Demo scripts -->
    {{-- <script src="../js/dashboard/dashboard-3.js"></script>
    <script src="../js/logout.js"></script> --}}



	<!-- Svganimation scripts -->
    <script src="{{ asset('resources/admin/vendor/svganimation/vivus.min.js')}}"></script>
    <script src="{{ asset('resources/admin/vendor/svganimation/svg.animation.js')}} "></script>
    <script src="{{ asset('resources/admin/js/styleSwitcher.js')}}"></script>


</body>
</html>
