<!-- ======= Header ======= -->
<header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <a href="{{ route('landing.index')}} " class="logo d-flex align-items-center">
        <img src="{{ asset('resources/home/images/favicon-pawican.png')}}" alt="">
        <span>{{ config('app.name') }}</span>
      </a>

      <nav id="navbar" class="navbar">
        <ul>
        @guest
        <li>
            <a class="nav-link scrollto {{ request()->routeIs('landing.index') ? 'active' : '' }}" href="{{ route('landing.index') }}">Home</a>
        </li>
        <li>
            <a class="nav-link scrollto {{ request()->routeIs('features.index') ? 'active' : '' }}" href="{{ route('features.index') }}">Features</a>
        </li>
        <li>
            <a class="nav-link scrollto {{ request()->routeIs('services.index') ? 'active' : '' }}" href="{{ route('services.index') }}">Services</a>
        </li>
        <li>
            <a class="nav-link scrollto {{ request()->routeIs('privacypolicy.index') ? 'active' : '' }}" href="{{ route('privacypolicy.index') }}">Privacy & Policy</a>
        </li>
        <li>
            <a class="getstarted scrollto {{ request()->routeIs('login') ? 'active' : '' }}" href="{{ route('login') }}">Login</a>
        </li>
        <li>
            <a class="getstarted2 scrollto {{ request()->routeIs('signup.create') ? 'active' : '' }}" href="{{ route('signup.create') }}">Sign Up</a>
        </li>

        @else
          <li><a class="nav-link scrollto active" href="{{ route('landing.index') }}">Home</a></li>
          <li><a class="nav-link scrollto" href="{{ route('features.index')}}">About</a></li>
          <li><a class="nav-link scrollto" href="{{ route('privacypolicy.index')}}">Volunteer</a></li>
          <li><a class="nav-link scrollto" href="{{ route('services.index')}}">Awareness</a></li>
          <li><a class="nav-link scrollto" href="{{ route('privacypolicy.index')}}">Trainings</a></li>
          <li><a class="nav-link scrollto" href="{{ route('privacypolicy.index')}}">Program</a></li>
          <li><a class="nav-link scrollto" href="{{ route('privacypolicy.index')}}">News</a></li>
            <li>
              <form id="logout-form" action="{{ route('auth.logout') }}" method="POST">
                @csrf
                <button type="submit" class="getstarted">Logout</button>
              </form>
            </li>
          @endguest
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->
