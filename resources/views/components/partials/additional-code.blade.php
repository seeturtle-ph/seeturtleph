<style>
    .toggle-password {
        cursor: pointer;
        position: absolute;
        right: 20px;
        top: 45px;
    }

    .form-group {
        position: relative;
    }
</style>
<script>
    $(document).ready(function() {
        toastr.options = {
            "closeButton": true,
            "progressBar": true,
            "positionClass": "toast-bottom-right",
            "timeOut": "5000"
        };

        // Display toastr messages from Laravel session
        @if(session()->has('success'))
            toastr.success('{{ session('success') }}');
        @elseif(session()->has('error'))
            // toastr.error('{{ session('') }}');
        @elseif(session()->has('warning'))
            toastr.warning('{{ session('warning') }}');
        @endif
    });
</script>

<script>
    function togglePassword(passwordId, toggleId) {
      var passwordField = document.getElementById(passwordId);
      var toggleIcon = document.getElementById(toggleId);
      if (passwordField.type === "password") {
        passwordField.type = "text";
        toggleIcon.classList.remove("fa-eye-slash");
        toggleIcon.classList.add("fa-eye");
      } else {
        passwordField.type = "password";
        toggleIcon.classList.remove("fa-eye");
        toggleIcon.classList.add("fa-eye-slash");
      }
    }
  </script>
<script>
    $(document).ready(function() {
        $(document).on('submit', 'form', function() {
            $('button').attr('disabled', 'disabled');
        });
    });
    </script>
</body>
