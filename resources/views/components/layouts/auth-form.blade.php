<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('resources/forms/fonts/icomoon/style.css')}}">
  <link rel="stylesheet" href="{{ asset('resources/forms/css/owl.carousel.min.css')}}">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('resources/forms/css/bootstrap.min.css')}}">
  <!-- Style -->
  <link rel="stylesheet" href="{{ asset('resources/forms/css/style.css') }}">
  <!-- font awsome -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">

  <!-- Toastr Flasher -->
  <link rel="stylesheet" href="{{ asset('toastr/css/toastr.min.css') }}">

  @if(isset($title))
  <title>{{ $title }}</title>
  @endif

</head>
<body>

  @yield('content')

  <script src="{{ asset('resources/forms/js/jquery-3.3.1.min.js') }}"></script>
  <script src="{{ asset('resources/forms/js/popper.min.js') }}"></script>
  <script src="{{ asset('resources/forms/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('resources/forms/js/main.js') }}"></script>
  <script src="{{ asset('resources/forms/js/address.js')}}"></script>
  <script src="{{ asset('toastr/js/toastr.min.js')}}"></script>
  <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js') }}"></script>

  @if (session('message'))
  <script>
    toastr.success("{{ session('message') }}");
  </script>
  @endif

  @if (session('warning'))
  <script>
  toastr.warning("{{ session('warning') }}");
  </script>
  @endif

  @if (session('error'))
  <script>
    toastr.error("{{ session('error') }}");
  </script>
  @endif

  @include('components.partials.additional-code')
</body>

</html>
