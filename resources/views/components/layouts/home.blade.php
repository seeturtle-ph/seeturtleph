<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  @if(isset($title))
  <title>{{ $title }}</title>
  @endif
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('resources/home/img/favicon.png') }}" rel="icon">
  <link href="{{ asset('resources/home/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('resources/home/vendor/aos/aos.css') }}" rel="stylesheet">
  <link href="{{ asset('resources/home/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('resources/home/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
  <link href="{{ asset('resources/home/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
  <link href="{{ asset('resources/home/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ asset('resources/home/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">

  <!-- Toastr CSS -->
  <link rel="stylesheet" href="{{ asset('toastr/css/toastr.min.css') }}">

  <!-- Template Main CSS File -->
  @if (isset($css))
  <link href="{{ asset('resources/home/css/'.$css.'/style.css') }}" rel="stylesheet">
  @endif

  <!-- =======================================================
  * Template Name: FlexStart
  * Template URL: https://bootstrapmade.com/flexstart-bootstrap-startup-template/
  * Updated: Mar 17 2024 with Bootstrap v5.3.3
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
  @include('components.partials.header')
  @yield('content')

  <!-- ======= Hero Section ======= -->


  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ asset('resources/home/vendor/purecounter/purecounter_vanilla.js') }}"></script>
  <script src="{{ asset('resources/home/vendor/aos/aos.js') }}"></script>
  <script src="{{ asset('resources/home/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('resources/home/vendor/glightbox/js/glightbox.min.js') }}"></script>
  <script src="{{ asset('resources/home/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('resources/home/vendor/swiper/swiper-bundle.min.js') }}"></script>
  <script src="{{ asset('resources/home/vendor/php-email-form/validate.js') }}"></script>

  <!-- jQuery -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>

  <!-- Toastr JS -->
  <script src="{{ asset('toastr/js/toastr.min.js') }}"></script>

  <!-- JS Notification Session -->
  @stack('scripts')

  <!-- Template Main JS File -->
  <script src="{{ asset('resources/home/js/main.js') }}"></script>

  <!-- Toastr Notification Script -->
</body>

</html>
