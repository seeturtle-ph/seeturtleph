<?php

use App\Http\Controllers\Auth\AuthenticationController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\SignUpController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\Web\AdminDashboard;
use App\Http\Controllers\Web\FeatureController;
use App\Http\Controllers\Web\LandingController;
use App\Http\Controllers\Web\PrivacyPolicyController;
use App\Http\Controllers\Web\ServiceController;
use App\Http\Controllers\Web\UserLandingController;
use Illuminate\Support\Facades\Route;

Route::controller(LandingController::class)->group(function(){

    Route::get('/', 'index')
    ->name('landing.index');
});

Route::controller(FeatureController::class)->group(function(){

    Route::get('/home/features', 'index')
    ->name('features.index');
});

Route::controller(ServiceController::class)->group(function(){

    Route::get('/home/services', 'index')
    ->name('services.index');
});

Route::controller(PrivacyPolicyController::class)->group(function(){

    Route::get('/home/privacy-policy', 'index')
    ->name('privacypolicy.index');
});

Route::controller(LoginController::class)->group(function(){

    Route::get('/users/login', 'index')
    ->name('login');

    Route::post('/users/authenticate', 'authenticate')
    ->name('login.authenticate');
});

Route::controller(AuthenticationController::class)->group(function(){

    Route::post('/users/logout', 'logout')
    ->name('auth.logout');

});

Route::controller(SignUpController::class)->group(function(){

    Route::get('/users/sign-up', 'create')
    ->name('signup.create');

    Route::post('/users/sign-up', 'store')
    ->name('signup.store');

});

Route::controller(VerifyEmailController::class)->group(function(){

    Route::get('/users/verify', 'index')
    ->name('verify-email.index');
});

Route::controller(VerificationController::class)->group(function(){

    Route::get('/users/verified-successful', 'index')
    ->name('verification.index');

    Route::get('/users/verify-email', 'verify')
    ->name('verification.verify');
});


Route::middleware('auth')->group(function(){

    //User Auth Group
    Route::controller(UserLandingController::class)->group(function(){

        Route::get('/users/landing-page', 'index')
        ->name('user-landing.index');


    });


    //Admin Auth Group
    Route::controller(AdminDashboard::class)->group(function(){

        Route::get('/admins/landing-page', 'index')
        ->name('admin-dashboard.index');
    });

});
