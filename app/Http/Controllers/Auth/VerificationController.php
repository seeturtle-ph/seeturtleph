<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class VerificationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function verify(Request $request)
    {
        $verificationCode = $request->query('code');
        $email = $request->query('email');

        $user = User::where('email', $email)->first();

        if ($user) {
            if ($user->email_verified_at) {
                return redirect()->route('login')->with('warning', 'Email is already verified.');
            } else if (Hash::check($verificationCode, $user->verification_code)) {
                $user->update([
                    'email_verified_at' => now(),
                    // 'verification_code' => null,
                ]);
                return redirect()->route('login')->with('message', 'Email verified successfully.');
            } else {
                return redirect()->route('login')->with('error', 'Invalid verification link.');
            }
        } else {
            return redirect()->route('signup.create')->with('error', 'Invalid verification link or already verified.');
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
