<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\SignUpFormRequest;
use App\Mail\VerifyEmail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class SignUpController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {   
        // Check if any user is logged in
        if (Auth::check()) {
            // Get the authenticated user
            $user = Auth::user();

            // Check the user's account type and redirect accordingly
            switch ($user->account_type) {
                case 'admin':
                    return redirect()->route('admin.dashboard');
                case 'government':
                    return redirect()->route('government.dashboard');
                case 'staff':
                    return redirect()->route('staff.dashboard');
                case 'superadmin':
                    return redirect()->route('superadmin.dashboard');
                case 'user':
                default:
                    return redirect()->route('user-landing.index');
            }
        } else {
            // If no user is logged in, return the login view
            return view('auth.signup.create', ['title' => 'Sign Up']);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SignUpFormRequest $request)
    {
        try {
            // Validate the incoming request
            $validatedData = $request->validated();

            // Check if the email already exists
            $existingUser = User::where('email', $validatedData['email'])->first();
            if ($existingUser) {
                // Redirect back to the registration form with a warning message
                return redirect()->back()->withInput()->with('warning', 'The account is already existing. Please try using another email address.');
            }

            // Calculate age from birthdate
            $birthdate = $validatedData['birthdate'];
            $age = Carbon::parse($birthdate)->age;
            $validatedData['age'] = $age;

            // Generate a random verification code
            $verificationCode = Str::random(40);

            // Hash the verification code
            $hashedVerificationCode = Hash::make($verificationCode);

            $validatedData['verification_code'] = $hashedVerificationCode;

            // Set default account type if not provided
            if (!isset($validatedData['account_type'])) {
                $validatedData['account_type'] = 'user';
            }

            // Create the user
            DB::transaction(function () use ($validatedData) {
                User::create($validatedData);
            });

            // Send verification email
            Mail::to($validatedData['email'])->send(new VerifyEmail($verificationCode, $validatedData['email']));
            // Redirect to the email verification page
            return redirect()->route('verify-email.index')->with('success', 'Account successfully saved! Check your email for verification.');

        } catch (\Exception $e) {
            // Redirect back to the registration form with the old input
            return redirect()->back()->withInput()->with('error', 'Something went wrong. Please try again.');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
