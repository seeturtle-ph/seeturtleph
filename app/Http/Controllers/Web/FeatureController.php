<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // Check if any user is logged in
        if (Auth::check()) {
            // Get the authenticated user
            $user = Auth::user();
            // Check the user's account type and redirect accordingly
            switch ($user->account_type) {
                case 'admin':
                    return redirect()->route('admin-dashboard.index');
                case 'government':
                    return redirect()->route('government.dashboard');
                case 'staff':
                    return redirect()->route('staff.dashboard');
                case 'superadmin':
                    return redirect()->route('superadmin.dashboard');
                case 'user':
                default:
                    return redirect()->route('user-landing.index');
            }
        } else {
            // If no user is logged in, return the login view
            return view('home.features.index', ['css' => 'home', 'title' => 'Features']);
        }
     }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
