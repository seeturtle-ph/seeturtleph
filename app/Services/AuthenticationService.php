<?php

namespace App\Services;

use App\Http\Requests\AuthenticateLoginFormRequest;
use App\Models\User;

class AuthenticationService
{

    public static function prepare()
    {
        return new self();
    }

    public function authenticate(User $user)
    {
        auth()->login($user, true);

        // generate token
        request()->session()->regenerate();

        return $this->redirectAfterLogin();
    }

    public function redirectAfterLogin()
    {
        $redirectUrl = 'login';

        if (auth()->user()->type) {
            $redirectUrl = $this->getRedirectUrl();
        }

        return redirect()->intended(route($redirectUrl));
    }

    public function attempt(AuthenticateLoginFormRequest $request)
    {
        if (
            auth()->attempt($request->validated(), true)
        ) {
            return $this->redirectAfterLogin();
        }
        return redirect()->back()->withErrors(['message' => 'Invalid credentials.']);
    }

    public function getRedirectUrl($bypass = false)
    {
        $redirectUrl = '';

        if (auth()->user()->type || $bypass) {

            switch (auth()->user()->role) {

                case 'admin':
                    $redirectUrl = '';
                    break;

                case 'staff':
                    $redirectUrl = '';
                    break;

                case 'user':
                    $redirectUrl = '';
                    break;
            }
        }

        return $redirectUrl;
    }
}
